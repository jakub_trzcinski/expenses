<?php
/**
 * Created by PhpStorm.
 * User: jakubtrzcinski
 * Date: 30.11.2016
 * Time: 19:42
 */

namespace AppBundle\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="expense")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 *
 * @property-read int $id
 * @property float $change
 * @property string $info
 * @property string $tag
 */
class Expense
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="float", name="diff")
     */
    public $change;

    /**
     * @ORM\Column(type="string")
     */
    public $info;

    /**
     * @ORM\Column(type="string")
     */
    public $tag;

    /**
     * @var \DateTime $created
     * @ORM\Column(name="created_at", type="datetime")
     */
    public $createdAt;

    /**
     * @var DateTime $updated
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new DateTime();
    }
}

