<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Expense;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class DefaultController extends Controller
{
    /**
 * @Route("/expenses", name="postExpenses", methods={"POST"})
 */
    public function postAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $data = $request->request;

        $expense = new Expense();
        $expense->change = $data->get("change");
        $expense->info = $data->get("info");
        $expense->tag = $data->get("tag");

        $em = $this->getDoctrine()->getManager();
        $em->persist($expense);
        $em->flush();
        return new JsonResponse([]);
    }

    /**
     * @Route("/expenses", name="getExpenses", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $expenses = $em->getRepository(Expense::class)->findAll();
        return new JsonResponse($expenses);
    }
}
